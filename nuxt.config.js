import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import pkg from './package'

export default {
  mode: 'universal',

  env: {
    functions:
      process.env.NODE_ENV === 'production'
        ? `https://steven-nuxt-freelancer.netlify.app/.netlify/functions`
        : 'http://localhost:9000'
  },

  /*
   ** Headers of the page
   */
  head: {
    title: "Steven Clipson",
    noscript: [{ innerHTML: 'Sorry, this website requires JavaScript to work correctly.' }],
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ]
  },
  manifest: {
    name: 'Steven Clipson',
    short_name: 'Steven',
    lang: 'en'
  },
  icon:{
    iconSrc: 'static/icon.png'
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/base/Loading.vue',

  /*
   ** Global CSS
   */
  css: ['~/assets/style/app.styl'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/vuetify',
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    'nuxt-responsive-loader',
    '@nuxtjs/component-cache'
  ],

  /*
   ** nuxt-responsive-loader settings
   */
  responsiveLoader: {
    name: 'img/[hash:7]-[width].[ext]',
    min: 200, // minimum image width generated
    max: 1080, // maximum image width generated
    steps: 5, // five sizes per image will be generated
    placeholder: true, // no placeholder will be generated
    quality: 90, // images are compressed with medium quality
    adapter: require('responsive-loader/sharp')
  },

  /*
   ** Build configuration
   */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/
      //   })
      // }
    }
  }
}
