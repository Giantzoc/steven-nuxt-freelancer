import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import '@mdi/font/css/materialdesignicons.min.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#D3C3BC',
    secondary: '#BFADA5',
    accent: '#D8EBF1',
    info: '#5B5C79',
    // primary: colors.blue.darken2,
    // accent: colors.grey.darken3,
    // secondary: colors.amber.darken3,
    // info: colors.teal.lighten1,
    // warning: colors.amber.base,
    // error: colors.deepOrange.accent4,
    // success: colors.green.accent3
  },
  iconfont: 'mdi'
})
