# steven-nuxt-freelancer

> My feelancing website

Created via 

npx create-nuxt-app <project-name>

Theme taken from vuetify freelancer

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```


To run the lambda functions for dev:
``` bash
$ npm run functions:serve
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
